import './App.css';
import CarDealer from "./components/CarDealer";

function App() {
    return (
        <div className="App">
            <CarDealer />
        </div>
    );
}

export default App;