import React, {useState} from 'react';

const CarDealer = () => {
    const [customerPrice, setCustomerPrice] = useState('')
    // 고객에게 입력받은 가격
    const [recoCarName, setRecoCarName] = useState('')
    // 추천 차량 이름
    const [error, setError] = useState('')
    // 오류 (ex) 예산이 부족할 경우 출력할)

    // 차 데이터 목업임
    const cars = [
        {carName: "기아 EV9", price: 8397},
        {carName: "제네시스 일렉츠리파이드 G80", price: 8392},
        {carName: "기아 K9", price: 7744},
        {carName: "아이오닉 5N", price: 7600},
        {carName: "제네시스 G90 LWB", price: 16714},
        {carName: "기아 카니발 하이리무진", price: 9200},
        {carName: "제네시스 GV80 쿠페", price: 9190},
        {carName: "현대 포레스트", price: 8725},
    ]

    // 예산 입력받기
    const handleUserPriceChange = e => {
        // 이벤트 자동 주입
        setCustomerPrice(e.target.value)
        // 입력받은 값으로 바꿔치기
        // setCustomerPrice 이용해야됨
    }

    // 추천하기
    const recoCar = () => {
        // 차만 추천하면 되기 때문에 매개변수 받을 게 없음
        console.log('눌린 거 확인')

        // 예산 - 기타 금액
        const realPrice = customerPrice - (300 + 30 + 40)
        console.log(realPrice)

        const test = cars.sort((a, b) => a.price - b.price)
        console.log(test)

        if (realPrice >= test[0].price) {
            // realPrice보다 작은 차량들 가져오기
            const arrCar = cars.filter(car => car.price / 1.07 <= realPrice).sort((a, b) => b.price - a.price)
            console.log(arrCar)

            // 차량 뽑아서 취득세 계산해주기
            if (realPrice >= arrCar[0].price) {
                return setRecoCarName(arrCar[0].carName)
            } else {
                return setError("돈 모아서 다시 오세요.")
            }
        } else {
            setError("돈 모아서 다시 오세요.")
            console.log("울랄라")
        }

    }

    return (
        <div>
            <input type="number" value={customerPrice} onChange={handleUserPriceChange} />
            <button onClick={recoCar}>추천받기</button>
            {error ? <p>{error}</p> : <p>추천차량 : {recoCarName}</p>}
            {/* 오류 발생했을 경우 오류 메시지 출력 아니면 차량 추천 */}
        </div>
    )

}

export default CarDealer;