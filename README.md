<img src="/public/img/react.jpeg">

# 차 딜러

## 프로젝트 간단 설명
Open [http://localhost:3000](http://localhost:3000)

예산을 입력해보세요. <br>
그에 맞는 차를 추천해드리겠습니다. <br>
저희가 보험료, 취득세 등까지 모두 계산해드리니 걱정은 하지 마세요!

### 프로젝트 실행 화면

#### 추천 화면
<img src="/public/img/car-dealers-1.png">
예산에 맞는 차량을 추천해드립니다.

#### 예산이 부족할 경우
<img src="/public/img/car-dealers-2.png">
예산이 부족할 경우, 메시지를 출력합니다.

### 사용한 기술
- filter
- sort
- input
- Array